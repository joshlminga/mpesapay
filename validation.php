<?php

	// Load config
	include 'config.php';
	include 'insert.php';

	if (!is_null($mpesaapi_config) && is_array($mpesaapi_config)) {

		//Get header
	    header("Content-Type:application/json"); 

	    //get post data
	    $postDataJson = file_get_contents('php://input');

	    if (!isset($_GET["token"]))
	    {
	      echo "No security token passed";
	      exit();
	    }
	    if ($_GET["token"]!=$mpesaapi_config['security_token'])
	    {
	      echo "Invalid token passed";
	      exit();
	    }

	    //compare the returned amount and expected amount below
	    $postData = json_decode($postDataJson);

	    $amountToBePaid = 1;
	    if ($postData['TransAmount'] == $amountToBePaid) {
	    	//for testing purposes, save transaction data found in array postData to database with success status
	    	//NB: This is not the final transaction. Only save for testing purposes

			$title = 'mpesa_validation_success_'.rand();
			add_txn($title, $postDataJson);

	    	//mpesa transaction feedback
			echo '{"ResultCode":0, "ResultDesc":"Success", "ThirdPartyTransID": 0}';
	    }else{
	    	//for testing purposes, save transaction data found in array postData to database with success status
	    	//NB: This is not the final transaction. Only save for testing purposes

		 	$title = 'mpesa_validation_failed_'.rand();
			add_txn($title, $postDataJson);

	    	//mpesa transaction feedback
			echo '{"ResultCode":1, "ResultDesc":"Failed", "ThirdPartyTransID": 0}'; 
	    }
	}else{

		echo "<br /> ---------- Config Failed ---------- <br />";
		echo "Mpesa configuration was not found";
	}

?>
