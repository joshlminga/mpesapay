<?php 

	//Db connection
	include 'dbconnect.php';

	// Predata
	$mpesaapi_config = null;

	// Select Data
	$sql = "SELECT setting_value as value FROM settings where setting_title = 'mpesa_api' limit 1";
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		$data_row = $result->fetch_assoc(); //Row
		$mpesaapi_config = json_decode($data_row['value'], True);
	}

?>