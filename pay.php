<?php 

    echo "New 25 <br/>";

	// Load config
	include 'config.php';

	//This are values received from the form or collected from the database with payment details
	$billingData = array(
		'payable'=> 1,
		// 'mobile'=> '0717873111'
		'mobile'=> '0708549604'
	);

	// Phone number must start with 254 without +
	$phone_number = preg_replace('/^0/','254',$billingData['mobile']); // Phone Number
	$payable = (int) $billingData['payable']; //Make sure the amount is in integer format

	// Access Token
		$config['key'] = $mpesaapi_config['consumer_key'];
		$config['secret'] = $mpesaapi_config['consumer_secret'];
		$config['url'] = $mpesaapi_config['registration_url'];

	$consumerKey = $mpesaapi_config['consumer_key'];//Fill with your app Consumer Key
	$consumerSecret = $mpesaapi_config['consumer_secret']; // Fill with your app Secret

	$headers = ['Content-Type:application/json; charset=utf8'];

	$url = 'https://sandbox.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials';

	$curl = curl_init($url);
	curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($curl, CURLOPT_HEADER, FALSE);
	curl_setopt($curl, CURLOPT_USERPWD, $consumerKey.':'.$consumerSecret);
	$result = curl_exec($curl);
	$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	$result = json_decode($result);

	
	curl_close($curl);
	$access_token = $result->access_token;

	//You can pass null to retrieve config data from database
	$url = $mpesaapi_config['authentication_url'];

  	// Load Access
  	$curl = curl_init();
  	curl_setopt($curl, CURLOPT_URL, $url);
  	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json','Authorization:Bearer '.$access_token)); //

  	// Config
  	$shortcode = $mpesaapi_config['shortcode'];
  	$receiver =  $mpesaapi_config['receiver'];
  	$passkey = $mpesaapi_config['pass_key'];
  	$callback_url = $mpesaapi_config['confirmation_url'];
  	$account = $mpesaapi_config['account'];
  	$description = $mpesaapi_config['description']; //You can opt to put your own as per transaction
  	$timestamp = date("YmdHis",time());
  	$password = base64_encode($shortcode."".$passkey."".$timestamp);

  	// Curl Post
  	$curl_post_data = array(
    	"BusinessShortCode"=> $shortcode,
    	"Password"=> $password,
		"Timestamp"=> $timestamp,
	    "TransactionType"=> "CustomerPayBillOnline",
	    "Amount"=> $payable,
	    "PartyA"=> $phone_number,
	    "PartyB"=> $receiver,
	    "PhoneNumber"=> $phone_number,
	    "CallBackURL"=> 'https://dev.topcar.co.ke/mpayment/confirmation.php?token=PR6342opQ3FyuH2u',
	    "AccountReference"=> $account,
	    "TransactionDesc"=> $description
  	);

  	$data_string = json_encode($curl_post_data);
  
  	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  	curl_setopt($curl, CURLOPT_POST, true);
  	curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
  
  	$curl_response = curl_exec($curl);
  	$response = (array) json_decode($curl_response);

  	// Check Callback - Response
  	if (array_key_exists('ResponseCode', $response)) {
  		echo "MPESA - PUSH was successful <br />";
  		echo "<pre>";
  		print_r($response);
  	}else{
  		echo "MPESA - PUSH failed <br />";
  		echo "<pre>";
  		print_r($response);
  	}

?>