<?php

	// Load config
	include 'config.php';
	include 'insert.php';

	if (!is_null($mpesaapi_config)) {
		if (is_array($mpesaapi_config)) {

			//Get header
		    header("Content-Type:application/json"); 

		    //get post data
		    $postDataJson = file_get_contents('php://input');

		    if (!isset($_GET["token"])){
		      echo "No security token passed";
		      exit();
		    }
		    if ($_GET["token"]!=$mpesaapi_config['security_token']){
		      echo "Invalid token passed";
		      exit();
		    }

		    if (!$request=file_get_contents('php://input')){
		        echo "Invalid input";

		    	//mpesa transaction feedback as failed
				echo '{"ResultCode":1, "ResultDesc":"Failed", "ThirdPartyTransID": 0}'; 
				exit();
		    }else{

			    //compare the returned amount and expected amount below
			    $postData = json_decode($postDataJson);
		    	//save transaction data found in array postData to database with success status
		    	//NB: This is the final payment record after transaction complete.

				$title = 'mpesa_confirmation_success_'.rand();
				add_txn($title, $postDataJson);

		    	//mpesa transaction feedback
				echo '{"ResultCode":0, "ResultDesc":"Success", "ThirdPartyTransID": 0}';
		    }
		}
	}

?>