-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 06, 2020 at 02:09 PM
-- Server version: 5.6.41-84.1
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bimakeny_live_site`
--

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `setting_id` bigint(20) NOT NULL,
  `setting_title` varchar(200) NOT NULL,
  `setting_value` longtext,
  `setting_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `setting_default` varchar(5) DEFAULT 'yes',
  `setting_flg` int(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`setting_id`, `setting_title`, `setting_value`, `setting_stamp`, `setting_default`, `setting_flg`) VALUES
(1, 'site_title', 'Bima Kenya Insurance Portal', '2019-06-03 04:19:15', 'yes', 1),
(2, 'site_slogan', 'Bima Kenya', '2019-11-21 14:21:58', 'yes', 1),
(3, 'theme_title', 'starter', '2018-11-23 14:19:36', 'yes', 1),
(4, 'site_status', 'online', '2018-12-17 08:52:06', 'yes', 1),
(5, 'offline_message', 'We are offline', '2018-12-17 08:50:58', 'yes', 1),
(6, 'current_url', 'title', '2018-12-17 17:54:02', 'yes', 1),
(7, 'mail_protocol', 'smtp', '2019-08-06 15:48:08', 'yes', 1),
(8, 'smtp_host', 'mail.bimakenya.com', '2020-03-31 22:45:51', 'yes', 1),
(9, 'smtp_user', 'system@bimakenya.com', '2020-03-31 22:45:51', 'yes', 1),
(10, 'smtp_pass', 'ipI@oFZ^l=h?', '2020-03-31 22:45:51', 'yes', 1),
(11, 'smtp_port', '465', '2020-03-31 22:45:51', 'yes', 1),
(12, 'smtp_timeout', '5', '2018-12-17 15:27:21', 'yes', 1),
(13, 'smtp_crypto', '', '2018-12-17 15:25:41', 'yes', 1),
(14, 'wordwrap', 'TRUE', '2018-12-17 15:27:10', 'yes', 1),
(15, 'wrapchars', '76', '2018-12-17 15:27:03', 'yes', 1),
(16, 'mailtype', 'html', '2020-03-31 22:45:51', 'yes', 1),
(17, 'charset', 'UTF-8', '2018-12-17 15:26:34', 'yes', 1),
(18, 'home_display', 'blog', '2018-12-17 17:24:53', 'yes', 1),
(19, 'home_post', 'latest_post', '2019-03-21 17:09:47', 'yes', 1),
(20, 'home_page', '', '2018-12-17 16:03:08', 'yes', 1),
(21, 'post_per_page', '10', '2018-12-17 16:11:11', 'yes', 1),
(22, 'post_show', 'summary', '2019-03-21 16:53:57', 'yes', 1),
(23, 'seo_visibility', 'noindex, nofollow', '2019-03-18 16:26:50', 'yes', 1),
(24, 'seo_global', 'any', '2019-03-18 16:37:13', 'yes', 1),
(25, 'seo_description ', '', '2018-12-17 16:09:31', 'yes', 1),
(26, 'seo_keywords', '', '2018-12-17 17:30:41', 'yes', 1),
(27, 'seo_meta_data', '', '2018-12-17 16:10:23', 'yes', 1),
(28, 'inheritance_data', 'default,category,tag,medical_limit,medical_benefit_type,medical_benefit_placed,product_for,user_title,occupation,nationality,gender,location,relationship,motor_coverage,motor_type,motor_use,travelplan,home_cover,type_of_building,homeplan,golfer_limit,personalaccident_limit,lifeinsurance_limit,id_type,marital_status,medical_covered,medical_benefits_shared,motor_optional_benefit,vehicle_type,life_optional_benefits,life_cover_type,construction_type,anti_theft_device,license,payment_installment,destinations,roof_type,wall_type,house_limit,motor_tonnage', '2019-12-19 19:01:57', 'yes', 1),
(29, 'module_list', 'main,blog,page,autofield,control,inheritance,customfield,user,level,setting,profile,userdatas,customers,company,insurancesetup,personalmedical,personalmotor,personaltravel,personalhome', '2019-07-15 12:36:58', 'yes', 1),
(30, 'extension_menu', '{\"menu_path\":\"customers/menu\"}', '2019-05-14 09:35:58', 'yes', 1),
(31, 'field_menu', '{\"menu_path\":\"companies/menu\"}', '2019-05-14 11:09:36', 'yes', 1),
(32, 'default_currency', 'KES', '2020-02-23 10:19:47', 'yes', 1),
(33, 'training_levy', '0.20%', '2019-12-23 18:08:13', 'yes', 1),
(34, 'phcf', '0.25%', '2019-10-29 17:37:55', 'yes', 1),
(35, 'stamp_duty', '40', '2019-10-29 17:37:52', 'yes', 1),
(36, 'administration_fee', '250', '2020-02-23 10:19:31', 'yes', 1),
(37, 'extension_menu', '{\"menu_path\":\"insurance/menu\"}', '2019-05-14 16:03:50', 'yes', 1),
(38, 'field_menu', '{\"menu_path\":\"product/personal/menu\"}', '2019-05-26 10:19:20', 'yes', 1),
(39, 'extension_menu', '{\"menu_path\":\"benefits/personal/menu\"}', '2019-05-26 16:47:19', 'yes', 1),
(59, 'assets', 'assets/admin', '2019-06-19 04:08:09', 'yes', 1),
(60, 'ext_dir', 'extend/', '2019-06-19 04:08:09', 'yes', 1),
(61, 'ext_assets', 'assets/extend', '2019-06-19 04:08:09', 'yes', 1),
(62, 'theme_name', 'bimainsurance', '2019-10-11 09:54:39', 'yes', 1),
(63, 'theme_dir', 'themes/bimainsurance', '2019-10-11 09:45:25', 'yes', 1),
(64, 'theme_assets', 'assets/themes/bimainsurance', '2019-10-11 09:45:29', 'yes', 1),
(65, 'child_theme', NULL, '2019-06-19 04:09:35', 'yes', 1),
(66, 'child_theme_dir', NULL, '2019-06-19 04:09:32', 'yes', 1),
(67, 'child_theme_assets', NULL, '2019-06-19 04:09:29', 'yes', 1),
(68, 'extension_menu', '{\"menu_path\":\"list_options/menu\"}', '2019-07-08 07:04:51', 'yes', 1),
(69, 'excise_duty', '', '2019-10-29 17:33:31', 'yes', 1),
(70, 'vat', '', '2019-10-29 17:33:31', 'yes', 1),
(71, 'delivery_fee', '300', '2020-02-23 10:19:31', 'yes', 1),
(72, 'field_menu', '{\"menu_path\":\"banks/menu\"}', '2019-05-30 16:13:15', 'yes', 1),
(73, 'extension_menu', '{\"menu_path\":\"importdata/menu\"}', '2019-12-13 05:11:01', 'yes', 1),
(74, 'company_name', 'Bima Kenya Online', '2019-12-13 05:11:01', 'yes', 1),
(75, 'field_menu', '{\"menu_path\":\"product/takaful/menu\"}', '2019-05-30 16:13:15', 'yes', 1),
(76, 'control_menu', '{\"menu_path\":\"extend/controls/mpesa_api/menu\"}', '2019-12-13 05:11:01', 'yes', 1),
(77, 'mpesa_api', '{\"consumer_key\":\"GWploQE6bB1Qi5GRAiAFn4NGTJ9Aaj4o\",\"consumer_secret\":\"nnQgp8xxBwH1FEGA\",\"security_token\":\"PR6342opQ3FyuH2u\",\"registration_url\":\"https:\\/\\/sandbox.safaricom.co.ke\\/oauth\\/v1\\/generate?grant_type=client_credentials\",\"authentication_url\":\"https:\\/\\/sandbox.safaricom.co.ke\\/mpesa\\/stkpush\\/v1\\/processrequest\",\"pass_key\":\"bfb279f9aa9bdbcf158e97dd71a467cd2e0c893059b10f78e6b72ada1ed2c919\",\"receiver\":\"174379\",\"shortcode\":\"174379\",\"account\":\"BIMAKENYA\",\"validation_url\":\"https:\\/\\/mpesa.bimakenya.com\\/validation.php?token=PR6342opQ3FyuH2u\",\"confirmation_url\":\"https:\\/\\/mpesa.bimakenya.com\\/confirmation.php?token=PR6342opQ3FyuH2u\",\"description\":\"Mpesa Sub Domain\"}', '2020-07-07 02:55:16', 'yes', 1),
(78, 'mpesa_validation_passed_232605616', 'test data', '2020-07-06 10:59:55', 'yes', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`setting_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `setting_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
